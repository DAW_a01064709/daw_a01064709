<!DOCTYPE html>
<html lang="es-mx">

<html>
<head>
        <title>Laboratorio 9</title>
        <meta charset="UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <nav class="fixed-nav-bar">
        <ul>
            <li><a href="DAW.html">DAW</a></li>
            <li><a href="BD.html">Bases de Datos</a></li>
            <li><a href="InfoPersonal.html">Info Personal</a></li>
        </ul>
    </nav>>

    <div id="wrapper">
        <header>
            <H1>Laboratorio 9 - PHP</H1>
        </header>

        <main>
        <?php

        function promedio (array $arr) {
            $res = 0;
            $l = count ($arr);
            $length = $l;

            while($l--) {
                $res += $arr[$l];
            }

            return $res/$length;
        }

        function mediana (array $arr) {
            $l = count($arr);
            sort($arr);

            if($l%2==0) {
                $res = ($arr[$l/2-1]+$arr[$l/2])/2;
            }
            else {
                $res = $arr[$l/2];
            }

            return $res;
        }

        function lista (array $arr) {
            $lista = "<ul><li>".implode(", ", $arr)."</li><li>".promedio($arr)."</li><li>".mediana($arr);

            sort($arr);

            $lista = $lista."</li><li>".implode(", ", $arr)."</li><li>";

            rsort($arr);

            $lista = $lista.implode(", ", $arr)."</li></ul>";

            return $lista;
        }

        function tabla ($num) {
            $tabla = "<table>";

            for ($i=1; $i <= $num; $i++) {
                $tabla=$tabla."<tr><td>".$i."</td>";
                $tabla=$tabla."<td>".pow($i,2)."</td>";
                $tabla=$tabla."<td>".pow($i,3)."</td></tr>";
            }

            $tabla=$tabla."</table>";

            return $tabla;
        }

        function libre ($num, $height, array $arr) {
            $length = count($arr);
            $res = 0;

            while($length--) {
                if($arr[$length]>$height)
                    $res+=2;
                else
                    $res++;
            }

            return $res;
        }

        $arr = array("4","8","1","5","7");
        $num = 5;
        $height = 5;

        echo "<H2>Ejercicio 1</H2> <br>".promedio($arr);
        echo "<br><H2>Ejercicio 2</H2> <br>".mediana($arr);
        echo "<br><H2>Ejercicio 3</H2> <br>".lista($arr);
        echo "<br><H2>Ejercicio 4</H2> <br>".tabla($num);
        echo "<br><H2>Ejercicio 5</H2> <p>http://codeforces.com/problemset/problem/677/A</p><p>Input</p><p>".$num." ".$height."</p><p>".implode(", ", $arr)."</p><br>Output: <br>".libre($num, $height, $arr);
        ?>

        </main>

        <footer id="contact">
            <h2>Sigueme:</h2>
                <nav class="nav_plain">
                    <ul>
                        <li><a href="https://www.facebook.com/lopez.david96"><i class="fa fa-facebook fa-3x"></i></a></li>
                    </ul>
                </nav>
        </footer>

    </div> 
</body>
</html>