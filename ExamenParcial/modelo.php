<?php

function conectar() {
    $mysql = mysqli_connect("localhost","david26","","ExamenParcial");
    $mysql->set_charset("utf8");
    return $mysql;
}

function desconectar($mysql) {
    mysqli_close($mysql);
}

function getTotalRegistrados() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT count(nombre) as total FROM zombie';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    $res = 'Total de zombies registrados <br>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$res .= '<h5>'.$fila["total"].'</h5>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $res .= "";
    
    return $res;
}

function getZombiesEstado() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT estado, count(estado) as total FROM zombie group by estado';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Estado</th>
              <th>Cantidad</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["estado"].'</td>
            <td>'.$fila["total"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}



function getNoMuertos() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT count(nombre) as total FROM zombie where zombie.estado != "completamente muerto"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    $res = 'Zombies no completamente muertos <br>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$res .= '<h5>'.$fila["total"].'</h5>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $res .= "";
    
    return $res;
}

function getEstado() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM estado';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '
    <select name="estado">
        <option value="" disable="" selected="">Estado</option>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
            <option value="'.$fila["nombre"].'">'.$fila["nombre"].'</option>';
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</select>";
    
    return $table;
}

function getZombies() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM zombie order by fechaR';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<h2>Registro de zombies</h2>
    <table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>ApellidoPaterno</th>
              <th>ApellidoMaterno</th>
              <th>Estado</th>
              <th>Fecha Registro</th>
              <th>Fecha Estado</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["nombre"].'</td>
            <td>'.$fila["apellidoP"].'</td>
            <td>'.$fila["apellidoM"].'</td>
            <td>'.$fila["estado"].'</td>
            <td>'.$fila["fechaR"].'</td>
            <td>'.$fila["fechaE"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}




function guardarRegistro($nombre, $apellidoP, $apellidoM, $estado){
    $db = conectar();
    
    // insert command specification
    $query='INSERT INTO zombie (`nombre`, `apellidoP` ,`apellidoM`, `estado`, `fechaE`, `fechaR`) VALUES (?,?,?,?,now(),now()) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ssss", $nombre, $apellidoP, $apellidoM, $estado)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     }
    
    desconectar($db);
}

function editarRegistro($nombre, $apellidoP, $apellidoM, $estado){
   $db=conectar();
   $sql = "UPDATE zombie SET estado='$estado', fechaE=now() WHERE nombre = '$nombre' and apellidoP = '$apellidoP' and apellidoM = '$apellidoM'";
   if(mysqli_query($db, $sql)){
    echo "Records were updated successfully.";
} else {
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($db);
}
   desconectar($db);
}