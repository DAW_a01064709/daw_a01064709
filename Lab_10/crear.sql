CREATE TABLE Materiales (
	Clave numeric(5) PRIMARY KEY,
	Descripcion varchar(50),
	Costo numeric(8,2)
)

CREATE TABLE Proovedores (
	RFC char(13) PRIMARY KEY,
	RazonSocial varchar(50)
)

CREATE TABLE Proyectos (
	Numero numeric(5) PRIMARY KEY,
	Denominación varchar(50)
)

CREATE TABLE Entregan (
	Clave numeric(5) FOREIGN KEY REFERENCES Materiales(Clave),
	RFC char(13) FOREIGN KEY REFERENCES Proovedores(RFC),
	Numero numeric(5) FOREIGN KEY REFERENCES Proyectos(Numero),
	Fecha datetime PRIMARY KEY,
	Cantidad numeric(8,2)
)