<?php
$nombre = "";
$apellido = "";
$telefono = "";
$email = "";
$msjErrNom = "";
$msjErrAp = "";
$msjErrTel = "";
$msjErrEmail = "";
$vNombre = false;
$vApellido = false;
$vTelefono = false;
$vEmail = false;
$res ="";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        if (empty($_POST["nombre"])) {
            $msjErrNom = "Nombre requerido";
        } 
        else {
            $nombre = test_input($_POST["nombre"]);
            if (!preg_match("/^[a-zA-Z ]*$/",$nombre)) {
              $msjErrNom = "Ingresa solo letras"; 
            }
            else {
                $vNombre=true;
            }
        }
        
        if (empty($_POST["apellido"])) {
            $msjErrAp = "Apellido requerido";
        } 
        else {
            $apellido = test_input($_POST["apellido"]);
            if (!preg_match("/^[a-zA-Z ]*$/",$apellido)) {
              $msjErrAp = "Ingresa solo letras"; 
            }
            else {
                $vApellido=true;
            }
        }

        if (empty($_POST["telefono"])) {
            $msjErrTel = "Campo obligatorio";
        } 
        else {
             $telefono = test_input($_POST["telefono"]);
             if (is_numeric($telefono)==false){
                 $msjErrTel = "Introduce un telefono válido";
             }
             $vTelefono=true;
        }
        
        if (empty($_POST["email"])) {
            $msjErrEmail = "Campo obligatorio";
        } 
        else {
            $email = test_input($_POST["email"]);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
              $msjErrEmail = "Introduce un e-mail válido"; 
            }
            else {
                $vEmail=true;
            }
        }
        
    }
    
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if ($vNombre == true and $vApellido == true and $vTelefono == true and $vEmail == true){
        $res = "Gracias por registrarte, nos pondremos en contacto contigo";
    }
?>