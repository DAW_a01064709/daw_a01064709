<?php
session_start();
?>

<!DOCTYPE html>
<html lang="es-mx">

<html>
<head>
        <title>Laboratorio 11</title>
        <meta charset="UTF-8">

              <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>
<body>
          <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
	<nav class="fixed-nav-bar">
        <ul class="right">
            <li><a href="DAW.html">DAW</a></li>
            <li><a href="BD.html">Bases de Datos</a></li>
            <li><a href="InfoPersonal.html">Info Personal</a></li>
        </ul>
	</nav>>
	<div id="wrapper">
		<header>
			<H1>Laboratorio 13</H1>
		</header>
		<main>
       <form class="col s12" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
              <div class="row">
	             <div class="row col s12">
	                <form action="DAW_Lab13_logout.php" method="post" enctype="multipart/form-data">
	                    <p> Bienvenido <?php echo $_SESSION["username"]; ?>: sube una foto</p>
	                    <div class="file-field input-field">
	                      <div class="btn">
	                        <span>Imagen</span>
	                        <input type="file" name="foto" id="foto">
	                      </div>
	                      <div class="file-path-wrapper">
	                        <input class="file-path validate" type="text">
	                      </div>
	                    </div>
	                    
	                    <div class="row margin">
	                        <div class="input-field col s12">
                      			<input class="waves-effect waves-light btn" type="submit" name="Submit" value="Submit" id="btnSubmit"/>
	                        </div>
	                    </div>
	                </form>
	            </div>
	              </div>
	          </form>
	                  <div class="col s6">
				        <img class="responsive-img circle" src="<?=$_SESSION["foto"] ?>">
				      </div>
				      	    <div class="input-field col s12">
	                         <a href="DAW_Lab13_logout.php">Log out</a>
	                        </div>
	        </main>

		<footer id="contact">
        	<h2>Sigueme:</h2>
            	<nav class="nav_plain">
                	<ul>
                    	<li><a href="https://www.facebook.com/lopez.david96"><i class="fa fa-facebook fa-3x"></i></a></li>
                	</ul>
            	</nav>
    	</footer>
	</div> 
</body>

</html>