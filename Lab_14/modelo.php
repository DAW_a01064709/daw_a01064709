<?php

function conectar() {
    $mysql = mysqli_connect("localhost","ejuarezp","","mascotas");
    $mysql->set_charset("utf8");
    return $mysql;
}

function desconectar($mysql) {
    mysqli_close($mysql);
}

function getTabla() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM alumnos WHERE carrera="ISC"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>ApellidoPaterno</th>
              <th>ApellidoMaterno</th>
              <th>Edad</th>
              <th>Carrera</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Nombre"].'</td>
            <td>'.$fila["ApellidoP"].'</td>
            <td>'.$fila["ApellidoM"].'</td>
            <td>'.$fila["Edad"].'</td>
            <td>'.$fila["Carrera"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}

function getISC() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM alumnos WHERE carrera="ISC"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>ApellidoPaterno</th>
              <th>ApellidoMaterno</th>
              <th>Edad</th>
              <th>Carrera</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Nombre"].'</td>
            <td>'.$fila["ApellidoP"].'</td>
            <td>'.$fila["ApellidoM"].'</td>
            <td>'.$fila["Edad"].'</td>
            <td>'.$fila["Carrera"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}

?>