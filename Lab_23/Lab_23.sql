create table clientes_banca
(
	noCuenta varchar(5) primary key,
	nombre varchar(30),
	saldo numeric(10,2)
)

create table tipos_movimientos
(
	claveM varchar(2) primary key,
	descripcion varchar(30)
)

create table movimientos
(
	noCuenta varchar(5) FOREIGN KEY REFERENCES clientes_banca(noCuenta),
	claveM varchar(2) FOREIGN KEY REFERENCES tipos_movimientos(claveM),
	fecha datetime,
	monto numeric(10,2)
)

begin transaction prueba1
insert into clientes_banca values('001', 'Manuel Rios Maldonado', 9000);
insert into clientes_banca values('002', 'Pablo Perez Ortiz', 5000);
insert into clientes_banca values('003', 'Luis Flores Alvarado', 8000);
commit transaction prueba1

select * from clientes_banca

/*
    Se muestra todas las tuplas de clientes_banca
*/

begin transaction prueba2
insert into clientes_banca values('004','Ricardo Rios Maldonado',19000);
insert into clientes_banca values('005','Pablo Ortiz Arana',15000);
insert into clientes_banca values('006','Luis Manuel Alvarado',18000);

select * from clientes_banca

/*
    No se ejecuta la consulta, porque se están utilizando los recursos
*/

rollback transaction prueba2

/*
    se revierte la transacción y se eliminan los inserts que se habían hecho
*/

select * from clientes_banca

/*
    se ven los mismos registros que se veian antes de la transacción
*/

begin transaction prueba3
insert into tipos_movimientos values('A','Retiro Cajero Automatico');
insert into tipos_movimientos values('B','Deposito Ventanilla');
commit transaction prueba3


begin transaction prueba4
insert into movimientos values('001','A',getdate(),500);
update clientes_banca set saldo = saldo -500
where noCuenta='001'
commit transaction prueba4

select * from tipos_movimientos

select * from movimientos

select * from clientes_banca

begin transaction prueba5
insert into clientes_banca values('025','Rosa Ruiz Maldonado',9000);
insert into clientes_banca values('036','Luis Camino Ortiz',5000);
insert into clientes_banca values('001','Oscar Perez Alvarado',8000);
if @@ERROR = 0 and @@ROWCOUNT = 3
commit transaction prueba5
else
begin
print 'A transaction needs to be rolled back'
rollback transaction prueba5
end

select * from clientes_banca

/*
Es una manera de excepción, agarra el error ejecuta algo en caso de que se presente un error.
Ejecutan las 2 inserciones, en el tercer insert se presenta un error por lo que hace un rollback de la transaction.
No hubo una modificación en la tabla, no se agregaron los registros 005 y 006, el tercero no lo agregó porque tenía la misma llave primaria que otro registro.
 */

create procedure registrar_retiro_cajero
	@noCuenta  varchar(5),
	@monto     numeric(10,2)
as
	begin transaction retiro_cajero
	insert into movimientos values(@noCuenta,'A',getdate(),@monto);
	update clientes_banca set saldo = saldo - @monto where noCuenta = @noCuenta
	if @@ERROR = 0
	commit transaction retiro_cajero
	else
	begin
	print 'A transaction needs to be rolled back'
    rollback transaction retiro_cajero
	end
go

select * from clientes_banca
select * from movimientos

execute registrar_retiro_cajero '001', 500.00

select * from clientes_banca
select * from movimientos

create procedure registrar_deposito_ventanilla
	@noCuenta  varchar(5),
	@monto     numeric(10,2)
as
	begin transaction deposito_ventanilla
    insert into movimientos values(@noCuenta, 'B', getdate(), @monto);
	update clientes_banca set saldo = saldo + @monto where noCuenta = @noCuenta
	if @@ERROR = 0
    commit transaction registrar_deposito_ventanilla
	else
	begin
	print 'A transaction needs to be rolled back'
	rollback transaction registrar_deposito_ventanilla
	end
go

select * from clientes_banca
select * from movimientos

execute registrar_deposito_ventanilla '001', 500.00

select * from clientes_banca
select * from movimientos



